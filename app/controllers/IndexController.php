<?php
declare(strict_types=1);

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Phalcon\Http\Request;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Url as UrlValidator;

class IndexController extends ControllerBase
{

    public function getFormAction()
    {

    }

    public function postFormAction()
    {
        if (!$this->security->checkToken()) {
            return $this->response->redirect('/');
        }

        if (!$this->validateForm($this->request)) {
            $this->flashSession->error('Url parameter must be URL');
            return $this->response->redirect('/');
        }
        $url = $this->request->getPost('url');
        $json = $this->getShortUrl($url);
        if (isset($json->result) && isset($json->result->url)) {
            $this->flashSession->success('Success! New short url: http://domain.com/' . $json->result->url);
        } else if (isset($json->error) && isset($json->error->message)) {
            $this->flashSession->error('Error: ' . $json->error->message);
        }

        return $this->response->redirect('/result');
    }

    public function resultAction()
    {

    }

    /**
     * @param Request $request
     * @return bool
     */
    private function validateForm(Request $request): bool
    {
        $parameterName = "url";

        $validation = new Validation();
        $validation->add($parameterName, new UrlValidator());
        $messages = $validation->validate($request->getPost());
        return count($messages) == 0;
    }

    /**
     * @param $url
     * @return stdClass
     */
    private function getShortUrl($url): stdClass
    {
        $client = new Client();
        $response = $client->request(
            'POST',
            '127.0.0.1:8001', [
            RequestOptions::JSON => [
                'jsonrpc' => '2.0',
                'method' => 'url.create',
                'params' => ['url' => $url],
                'id' => 2
            ],
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $json = json_decode($response->getBody()->getContents());
        return $json;
    }

}

