<?php

$router = $di->getRouter();

$router->add('/', 'Index::getForm')->via(['GET']);
$router->add('/', 'Index::postForm')->via(['POST']);
$router->add('/result', 'Index::result')->via(['GET']);

$router->handle($_SERVER['REQUEST_URI']);
